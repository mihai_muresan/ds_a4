package com.onlinetracking.core.services;

import com.onlinetracking.core.dao.RouteRepository;
import com.onlinetracking.core.entities.CheckpointEntity;
import com.onlinetracking.core.entities.RouteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RouteService extends BaseEntityService {
    private final RouteRepository routeRepository;

    @Autowired
    public RouteService(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    @Transactional()
    public void addCheckpoint(RouteEntity routeEntity, CheckpointEntity checkpoint) {
        routeEntity.getCheckpointEntities().add(checkpoint);

        this.routeRepository.save(routeEntity);
    }
}
