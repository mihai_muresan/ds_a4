package com.onlinetracking.core.services;

import com.onlinetracking.core.dao.ClientRepository;
import com.onlinetracking.core.entities.ClientEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public void save(String name, String username, String password) {
        ClientEntity clientEntity = new ClientEntity();

        clientEntity.setName(name);
        clientEntity.setUsername(username);
        clientEntity.setPassword(password);

        this.clientRepository.save(clientEntity);
    }

    @Override
    public ClientEntity find(String username, String password) {
        return this.clientRepository.findByUsernameAndPassword(username, password);
    }
}
