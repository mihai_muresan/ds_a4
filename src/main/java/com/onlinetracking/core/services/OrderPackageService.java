package com.onlinetracking.core.services;

import com.onlinetracking.core.dao.OrderPackageRepository;
import com.onlinetracking.core.dao.RouteRepository;
import com.onlinetracking.core.entities.CheckpointEntity;
import com.onlinetracking.core.entities.OrderPackageEntity;
import com.onlinetracking.core.entities.RouteEntity;
import com.onlinetracking.ws.admin.OrderPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class OrderPackageService extends BaseEntityService {
    private final OrderPackageRepository orderPackageRepository;
    private final RouteRepository routeRepository;

    @Autowired
    public OrderPackageService(OrderPackageRepository orderPackageRepository, RouteRepository routeRepository) {
        this.orderPackageRepository = orderPackageRepository;
        this.routeRepository = routeRepository;
    }

    public OrderPackageEntity mapToEntity(OrderPackage orderPackage) {
        return this.mapper.map(orderPackage, OrderPackageEntity.class);
    }

    public OrderPackage mapFromEntity(OrderPackageEntity orderPackageEntity) {
        return this.mapper.map(orderPackageEntity, OrderPackage.class);
    }

    public void startTracking(OrderPackageEntity orderPackageEntity) {
        RouteEntity routeEntity = new RouteEntity();
        CheckpointEntity checkpointEntity = new CheckpointEntity();
        checkpointEntity.setCity(orderPackageEntity.getSenderCity());
        checkpointEntity.setTime(Calendar.getInstance());

        routeEntity.getCheckpointEntities().add(checkpointEntity);

        routeRepository.save(routeEntity);

        orderPackageEntity.setTracking(true);
        orderPackageEntity.setRouteEntity(routeEntity);

        this.orderPackageRepository.save(orderPackageEntity);
    }
}
