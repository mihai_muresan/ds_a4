package com.onlinetracking.core.services;

import com.onlinetracking.core.entities.ClientEntity;
import org.springframework.stereotype.Component;

@Component
public interface ClientService {
    void save(String name, String username, String password);

    ClientEntity find(String username, String password);
}
