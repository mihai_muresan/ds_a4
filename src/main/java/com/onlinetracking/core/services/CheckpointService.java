package com.onlinetracking.core.services;

import com.onlinetracking.core.entities.CheckpointEntity;
import com.onlinetracking.ws.admin.Checkpoint;
import org.springframework.stereotype.Component;

@Component
public class CheckpointService extends BaseEntityService {
    public CheckpointEntity mapToEntity(Checkpoint checkpoint) {
        return this.mapper.map(checkpoint, CheckpointEntity.class);
    }

    public Checkpoint mapFromEntity(CheckpointEntity checkpointEntity) {
        return this.mapper.map(checkpointEntity, Checkpoint.class);
    }
}
