package com.onlinetracking.core.services;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

public class BaseEntityService {
    protected final Mapper mapper;

    public BaseEntityService() {
        this.mapper = new DozerBeanMapper();
    }
}
