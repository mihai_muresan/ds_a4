package com.onlinetracking.core.entities;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity(name = "order_packages")
public class OrderPackageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    @Cascade(CascadeType.MERGE)
    private ClientEntity sender;
    @OneToOne
    @Cascade(CascadeType.MERGE)
    private ClientEntity receiver;
    private String name;
    private String description;
    private String senderCity;
    private String destinationCity;
    private boolean tracking;
    @OneToOne
    @Cascade(CascadeType.PERSIST)
    private RouteEntity routeEntity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClientEntity getSender() {
        return sender;
    }

    public void setSender(ClientEntity sender) {
        this.sender = sender;
    }

    public ClientEntity getReceiver() {
        return receiver;
    }

    public void setReceiver(ClientEntity receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public RouteEntity getRouteEntity() {
        return routeEntity;
    }

    public void setRouteEntity(RouteEntity routeEntity) {
        this.routeEntity = routeEntity;
    }
}
