package com.onlinetracking.core.entities;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "routes")
public class RouteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToMany(fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    private List<CheckpointEntity> checkpointEntities;

    /**
     * Gets the value of the id property.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the checkpointEntities property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the checkpointEntities property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCheckpointEntities().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckpointEntity }
     */
    public List<CheckpointEntity> getCheckpointEntities() {
        if (checkpointEntities == null) {
            checkpointEntities = new ArrayList<CheckpointEntity>();
        }
        return this.checkpointEntities;
    }
}
