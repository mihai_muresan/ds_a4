package com.onlinetracking.core.dao;

import com.onlinetracking.core.entities.OrderPackageEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OrderPackageRepository extends CrudRepository<OrderPackageEntity, Integer> {
    List<OrderPackageEntity> findAllBySender_IdOrReceiver_Id(int senderId, int receiverId);
}
