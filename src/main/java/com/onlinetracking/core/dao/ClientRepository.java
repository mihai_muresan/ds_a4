package com.onlinetracking.core.dao;

import com.onlinetracking.core.entities.ClientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ClientRepository extends CrudRepository<ClientEntity, Integer> {
    ClientEntity findByUsernameAndPassword(String username, String password);
}
