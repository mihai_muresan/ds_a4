package com.onlinetracking.core.dao;

import com.onlinetracking.core.entities.RouteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface RouteRepository extends CrudRepository<RouteEntity, Integer> {
}
