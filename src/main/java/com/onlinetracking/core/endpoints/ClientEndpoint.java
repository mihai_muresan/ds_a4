package com.onlinetracking.core.endpoints;

import com.onlinetracking.core.dao.OrderPackageRepository;
import com.onlinetracking.core.entities.ClientEntity;
import com.onlinetracking.core.entities.OrderPackageEntity;
import com.onlinetracking.core.services.CheckpointService;
import com.onlinetracking.core.services.ClientService;
import com.onlinetracking.core.services.OrderPackageService;
import com.onlinetracking.ws.admin.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class ClientEndpoint {
    private static final String NAMESPACE_URI = "http://onlinetracking.com/ws/admingit [";
    private ClientService clientService;
    private OrderPackageRepository orderPackageRepository;
    private OrderPackageService orderPackageService;
    private CheckpointService checkpointService;

    @Autowired
    public ClientEndpoint(ClientService clientService, OrderPackageRepository orderPackageRepository, OrderPackageService orderPackageService, CheckpointService checkpointService) {
        this.orderPackageRepository = orderPackageRepository;
        this.clientService = clientService;
        this.orderPackageService = orderPackageService;
        this.checkpointService = checkpointService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "registerRequest")
    @ResponsePayload
    public void registerRequest(@RequestPayload RegisterRequest registerRequest) {
        this.clientService.save(registerRequest.getName(), registerRequest.getUsername(), registerRequest.getPassword());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginRequest")
    @ResponsePayload
    public LoginResponse loginRequest(@RequestPayload LoginRequest loginRequest) {
        ClientEntity clientEntity = this.clientService.find(loginRequest.getUsername(), loginRequest.getPassword());
        LoginResponse loginResponse = new LoginResponse();

        if (clientEntity == null) {
            loginResponse.setSuccess(false);
        } else {
            loginResponse.setSuccess(true);
            loginResponse.setClientId(clientEntity.getId());
        }

        return loginResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "listPackagesRequest")
    @ResponsePayload
    public ListPackagesResponse listPackagesRequest(@RequestPayload ListPackagesRequest listPackagesRequest) {
        ListPackagesResponse listPackagesResponse = new ListPackagesResponse();

        this.orderPackageRepository.findAllBySender_IdOrReceiver_Id(listPackagesRequest.getClientId(), listPackagesRequest.getClientId())
                .forEach(orderPackageEntity -> listPackagesResponse.getPackages().add(this.orderPackageService.mapFromEntity(orderPackageEntity)));

        return listPackagesResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "showPackageStatusRequest")
    @ResponsePayload
    public ShowPackageStatusResponse showPackageStatusRequest(@RequestPayload ShowPackageStatusRequest showPackageStatusRequest) {
        ShowPackageStatusResponse showPackageStatusResponse = new ShowPackageStatusResponse();

        OrderPackageEntity orderPackageEntity = this.orderPackageRepository.findOne(showPackageStatusRequest.getPackageId());

        List<Checkpoint> checkpointList = showPackageStatusResponse.getCheckpoints();

        orderPackageEntity
                .getRouteEntity()
                .getCheckpointEntities()
                .forEach(checkpointEntity -> checkpointList.add(this.checkpointService.mapFromEntity(checkpointEntity)));

        return showPackageStatusResponse;
    }
}
