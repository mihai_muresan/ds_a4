package com.onlinetracking.core.endpoints;

import com.onlinetracking.core.dao.OrderPackageRepository;
import com.onlinetracking.core.entities.OrderPackageEntity;
import com.onlinetracking.core.services.CheckpointService;
import com.onlinetracking.core.services.OrderPackageService;
import com.onlinetracking.core.services.RouteService;
import com.onlinetracking.ws.admin.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class AdminEndpoint {
    private static final String NAMESPACE_URI = "http://onlinetracking.com/ws/admin";
    private final OrderPackageService orderPackageService;
    private final CheckpointService checkpointService;
    private OrderPackageRepository orderPackageRepository;
    private RouteService routeService;

    @Autowired
    public AdminEndpoint(@Qualifier("orderPackageRepository") OrderPackageRepository orderPackageRepository, OrderPackageService orderPackageService, RouteService routeService, CheckpointService checkpointService) {
        this.orderPackageRepository = orderPackageRepository;
        this.orderPackageService = orderPackageService;
        this.routeService = routeService;
        this.checkpointService = checkpointService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPackageRequest")
    @ResponsePayload
    public AddPackageResponse addPackage(@RequestPayload AddPackageRequest request) {
        OrderPackage orderPackage = request.getOrderPackage();
        AddPackageResponse addPackageResponse = new AddPackageResponse();

        OrderPackageEntity orderPackageEntity = this.orderPackageService.mapToEntity(orderPackage);

        orderPackageEntity = this.orderPackageRepository.save(orderPackageEntity);

        addPackageResponse.setOrderPackage(this.orderPackageService.mapFromEntity(orderPackageEntity));

        return addPackageResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "removePackageRequest")
    @ResponsePayload
    public void removePackage(@RequestPayload RemovePackageRequest request) {
        int orderPackageId = request.getOrderPackageId();

        this.orderPackageRepository.delete(orderPackageId);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "trackPackageRequest")
    @ResponsePayload
    public void trackPackage(@RequestPayload TrackPackageRequest trackPackageRequest) {
        OrderPackageEntity orderPackageEntity = this.orderPackageRepository.findOne(trackPackageRequest.getPackageId());
        this.orderPackageService.startTracking(orderPackageEntity);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPackageCheckpointRequest")
    @ResponsePayload
    public void addPackageCheckpoint(@RequestPayload AddPackageCheckpointRequest addPackageCheckpointRequest) {
        OrderPackageEntity orderPackageEntity = this.orderPackageRepository.findOne(addPackageCheckpointRequest.getPackageId());
        this.routeService.addCheckpoint(orderPackageEntity.getRouteEntity(), this.checkpointService.mapToEntity(addPackageCheckpointRequest.getCheckpoint()));
    }
}
